<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Marketing and web developer</title>
	<link rel="stylesheet" href="css/profile.css">
	<link rel="stylesheet" href="css/base.css">
	<link rel="stylesheet" href="css/foundation.min.css">
	<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
</head>
<body data-hijacking="off" data-animation="scaleDown" id="top">
	
	<!-- End #Preloader -->
<!-- BACK GROUND -->
	<section id="background">
		<div class="row fullWidth">
			<div class="small-12 columns relative parallax">
				<div class="myName absolute center">
					<h1>DRAKE LEE</h1>

				</div>
				<section id="home" class="s-home page-hero target-section" data-parallax="scroll" data-image-src="images/hero-bg.jpg" data-natural-width=3000 data-natural-height=2000 data-position-y=center>

        <div class="overlay"></div>
        <div class="shadow-overlay"></div>

        
			</div>

		</div>
	</section>
<!-- END BACK GROUND -->

<!-- HEADER -->
<header id="myHeader">
	<div class="row ">
		<div class="small-12 medium-12 large-3 columns padding-0">
			<h3 onclick="myFunction()" class="text-center nav-btn" >DRAKE LEE</h3>
		</div>
		<div id="myDropdown" class="small-12 medium-12 large-9 columns nav">
			<ul>
				<li>
					<a id="per" href="#" class="text-upp">Persional Details</a>
				</li>
				<li>
					<a id="soft" href="#" class="text-upp">Soft Skills</a>
				</li>
				<li>
					<a id="spec" href="#" class="text-upp">Specialized Skills</a>
				</li>
				<li>
					<a id="exp" href="#" class="text-upp">Experience</a>
				</li>
				<li>
					<a id="con" href="#" class="text-upp">Contact</a>
				</li>
			</ul>
		</div>
	</div>
</header>
<!-- END HEADER -->