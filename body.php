
<div id="perr"></div>
<!-- headline -->
<div class="row">
    <div class="small-12 text-center padding-100-0-0">
                        
        <div class="headline">
            <p><span>Who am I?</span></p>
            <h1>Personal Details</h1>
        </div>
                        
    </div>
</div>
<!-- End headline-->

<!-- PERSIONAL DETAILS -->

<section id="persionalDetails">
	<div class="row ">
		<div class="small-12 medium-6 large-3 columns relative padding-10" style="padding-top: 24px">
			<div class="avatar"></div>
		</div>
		<div class="small-12 medium-6 large-4 columns relative padding-10">
			<p>Name : Drake Lee</p>
			<p>Date of Birth : 11 November 1994</p>
			<p>Nationality : Ho Chi Minh - VietNam</p>
			<p>Address : 5/92 Hamlet 1 Dong Thanh Province Dang Thuc Vinh Street</p>
			<p>Phone : (+84) 90 168 2328</p>
			<p style="border-bottom: 0px solid #e1e1e1;">Email : drake.lpdk@gmail.com - dangkhuyenk@gmail.com</p>
		</div>
		<div class="small-12 medium-12 large-5 columns relative padding-10">
			<h3>More about me</h3>
			<p style="border-bottom: 0px solid #e1e1e1; line-height: 30px;">I am an extraordinary man. Because I was born a normal human, But I know how to connect the small details, necessary, and complete it the best way.<br/>I like travel <i class="fas fa-plane"></i>, discover myself <i class="fab fa-cc-discover"></i> and i love italian food <i class="fas fa-utensils"></i>.<br/>I do everything by my Heart and that made me different <i class="fas fa-heart"></i>.</p>
			<img src="img/sign2.png" alt="DRAKE LEE" width="200px" height="50px" style="transform: rotate(-4deg);">
		</div>
	</div>
</section>
<!-- -->


<div id="softt"></div>
<!-- headline -->
<div class="row">
    <div class="small-12 text-center padding-100-0-0">
                        
        <div class="headline">
            <p><span>Right Brain</span></p>
            <h1>Solf Skills</h1>
        </div>
                        
    </div>
</div>
<!-- End headline-->

<!-- Soft Skills -->

	<section id="softSkills">
		<div class="row">
			<div class="bars">

				   <ul class="skills">
					   <li><span id="pre" class="bar-expand"></span><em>Presentation</em></li>
                  <li><span id="itk" class="bar-expand itk"></span><em>Independence Thinking</em></li>
						<li><span id="ctk" class="bar-expand ctk"></span><em>Critical Thinking</em></li>
						<li><span  id="lea" class="bar-expand lea"></span><em>Working Group</em></li>
						<!--
						<li><span class="bar-expand html5"></span><em>HTML5</em></li>
                  <li><span class="bar-expand jquery"></span><em>jQuery</em></li> 
              -->
					</ul>

				</div>
			
		</div>
	</section>
<!-- End Soft Skills --> 

<div id="specc"></div>
<!-- headline -->
<div class="row">
    <div class="small-12 text-center padding-100-0-0">
                        
        <div class="headline">
            <p><span>Left Brain</span></p>
            <h1>Specialized Skills</h1>
        </div>
                        
    </div>
</div>
<!-- End headline-->


<!-- SPECIALIZED SKILLS -->

	<section id="specializedSkills">
		<!-- -->
		<div class="row">
			
			<div class="small-12 medium-4 columns">
				<div class="title-skills">BUSINESS ANALYST</div>
				<ul class="check">
					<li>Design Wireframe and Mock up</li>
					<li>Write User Stories ,Business Case, Use Case and Scenarious</li>
					<li>Good connect beetween Development Team and Third Party</li>
					<li>Tools : Dreamweaver 5 , Jira, Slack , Mirosoft Office</li>
				</ul>
			</div>
			<div class="small-12 medium-4 columns">
				<div class="title-skills">LANGUAGUES SKILL</div>
				<div class="circle text-center relative margin-30-auto"><p class="absolute center">AMERICAN ENGLISH</p></div>
			</div>
			<div class="small-12 medium-4 columns">
				<div class="title-skills">AGILE SKILLS</div>
				<ul class="check">
					<li>Scrum : Product/Sprint Backlog, Burn Down Chart, Increment</li>
					<li>Applying Kanban to work</li>
					<li>Tool : Jira, Confluence, Slack, Webex, Skype Business.</li>
				</ul>
			</div>
			</div>
			<!-- TECHNICAL -->
		<div class="row">
			<div class="title-skills">TECHNICAL SKILLS</div>
			
			<div class="small-6 medium-3 columns text-center">
				<div class="size-40 padding-15-0"><i class="fas fa-keyboard"></i></div>
				<h3 class="font1">Front-End Development</h3>
				<ul>
					<li>HTML5</li>
					<li>CSS3</li>
					<li>FOUNDATION5</li>
					<li>BOOSTRAP3</li>
					<li>JAVASCRIPT</li>
					<li>JQUERY</li>
				</ul>
			</div>
			<div class="small-6 medium-3 columns text-center" style="margin-bottom: 50px;">
				<div class="size-40 padding-15-0"><i class="fas fa-keyboard"></i></div>
				<h3 class="font1">Back-End Development</h3>
				<ul>
					<li>Java</li>
					<li>C#</li>
					<li>Python</li>
					<li>Php</li>
				</ul>
			</div>
			<div class="small-6 medium-3 columns text-center">
				<div class="size-40 padding-15-0"><i class="fab fa-microsoft"></i></div>
				<h3 class="font1">Tools</h3>
				<ul>
					<li>Photoshop</li>
					<li>Proshow</li>
					<li>Source Tree</li>
					<li>TortoiseSVN</li>
					<li>Bitbucket</li>
					<li>Jira</li>
					<li>Slack</li>
				</ul>
			</div>
			<div class="small-6 medium-3 columns text-center">
				<div class="size-40 padding-15-0"><i class="fas fa-database"></i></div>
				<h3 class="font1">DBMS System</h3>
				<ul>
					<li>SQL Server 2012</li>
					<li>My SQL</li>
				</ul>
			</div>
		</div>
			<!-- #Makerting -->
			<div class="row">
				<div class="title-skills">MARKETING SKILLS</div>
				<div class="small-6 medium-3 columns"><div class="square relative"><p class="absolute center text-center">SEO</p></div></div>
				<div class="small-6 medium-3 columns"><div class="square relative"><p class="absolute center text-center">Sales Funel Management</p></div></div>
				<div class="small-6 medium-3 columns"><div class="square relative"><p class="absolute center text-center">Digital Marketing</p></div></div>
				<div class="small-6 medium-3 columns"><div class="square relative"><p class="absolute center text-center">Traditional Marketing</p></div></div>
				<div class="small-6 medium-3 columns"><div class="square relative"><p class="absolute center text-center">Marketing and Sales Strategy</p></div></div>
				<div class="small-6 medium-3 columns"><div class="square relative"><p class="absolute center text-center">Email Marketing</p></div></div>
				<div class="small-6 medium-3 columns"><div class="square relative"><p class="absolute center text-center">Trade Event Planning</p></div></div>
				<div class="small-6 medium-3 columns"><div class="square relative"><p class="absolute center text-center">B2B & B2C</p></div></div>
			
			</div>
		</div>
	</section>
<!-- END SPECIALLIZED SKILLS -->

<div id="expp"></div>
<!-- headline -->
<div class="row">
    <div class="small-12 text-center padding-100-0-0">
                        
        <div class="headline">
            <p><span>My Background</span></p>
            <h1>Work Experience</h1>
        </div>
                        
    </div>
</div>
<!-- End headline-->

<!-- #Experience -->

<section id="experience">
	<div class="row">
		<div class="small-12 medium-3 columns">
			<div class="title-exp">
				<h1><span>2017<small>November</small></span><span>-</span><span>Present</span></h1>
				<h3>A Chau English Center</h3>
				<p>America English Teacher</p>
			</div>
		</div>
		<div class="small-12 medium-9 columns">
			<div class="content-exp">
				<h3 class="text-upp">Job Description</h3>
				<p><i class="fas fa-angle-double-right"></i> Teaching English combined with my America Culture has brought me the best RESULTS . Pronounciation is the most important skill I focus on.</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="small-12 medium-3 columns">
			<div class="title-exp">
				<h1><span>2017<small>March</small></span><span>-</span><span><small>November</small>2017</span></h1>
				<h3>Larion</h3>
				<p>Business Analyst</p>
			</div>
		</div>
		<div class="small-12 medium-9 columns">
			<div class="content-exp">
				<h3 class="text-upp">Job Description</h3>
				<p><i class="fas fa-angle-double-right"></i> By applying the AGILE AND SCRUM process, I connect best between product owner and the remaining components. <br/><i class="fas fa-angle-double-right"></i> Design wireframe and mockup. <br/> <i class="fas fa-angle-double-right"></i> Write user stories and print plans.</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="small-12 medium-3 columns">
			<div class="title-exp">
				<h1><span>2016<small>Agust</small></span><span>-</span><span>2017<small>March</small></span></h1>
				<h3>Capgemini VietNam</h3>
				<p>Software Engineer</p>
			</div>
		</div>
		<div class="small-12 medium-9 columns">
			<div class="content-exp">
				<h3 class="text-upp">Job Description</h3>
				<p><i class="fas fa-angle-double-right"></i> Making the insurance softwaer as a back-end developer .</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="small-12 medium-3 columns">
			<div class="title-exp">
				<h1><span>2016<small>January</small></span><span>-</span><span>2016<small>Agust</small></span></h1>
				<h3>FHH-GLOBAL</h3>
				<p>Software Engineer <br/> Saleman <br/> Marketing</p>
			</div>
		</div>
		<div class="small-12 medium-9 columns">
			<div class="content-exp">
				<h3 class="text-upp">Job Description</h3>
				<p>Software Engineer<br/><i class="fas fa-angle-double-right"></i> Design Template .</p>
				<p>Saleman<br/><i class="fas fa-angle-double-right"></i> Benefit the customers with quality products of the company - WEBSITE - HOSTING - SEO</p>
				<p>Marketing<br/><i class="fas fa-angle-double-right"></i> Planing to increase conversion rates for companies and personal.</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="small-12 medium-3 columns">
			<div class="title-exp">
				<h1><span>2014</span><span>-</span><span>2015</span></h1>
				<h3>Prudential</h3>
				<p>Telesale<br/>Saleman</p>
			</div>
		</div>
		<div class="small-12 medium-9 columns">
			<div class="content-exp">
				<h3 class="text-upp">Job Description</h3>
				<p>Telesale<br/><i class="fas fa-angle-double-right"></i> Reaching customers by calling</p>
				<p>Saleman<br/><i class="fas fa-angle-double-right"></i> Reaching the customers by traditional marketing and provided for them the best Service</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="small-12 medium-3 columns">
			<div class="title-exp">
				<h1><span>2012</span><span>-</span><span>2014</span></h1>
				<h3>Shop and GO 24h</h3>
				<p>Counter</p>
			</div>
		</div>
		<div class="small-12 medium-9 columns">
			<div class="content-exp">
				<h3 class="text-upp">Job Description</h3>
				<p><i class="fas fa-angle-double-right"></i> Charging customers.<br/><i class="fas fa-angle-double-right"></i> Check store receipts and work directly with the manager.</p>
			</div>
		</div>
	</div>
</section>
<!-- End #Experience-->

	