<?php 
	$link = new mysqli('localhost','root','','drakeleeprofile');
	mysqli_query($link,'SET NAMES UTF8');
?>
	
<!-- Contact -->
<div id="conn"></div>
<section id="contact">
	<div class="row fullWidth">
		<div class="small-12">
			<div class="parallax_overlay">

				<!-- headline -->
				<div class="row">
				    
				        <div class="small-12 text-center padding-100-0-0">                
				        <div class="myName">
				            <h1>CONTACT</h1>
				        </div>
				        </div>
				        <form method="POST">
					        <section class="content bgcolor-1">
						        <div class="small-12 text-center ">
									<span class="input input--minoru">
									    <input class="input__field input__field--yoko" type="text" id="input-16" name="fName" >
									    <label class="input__label input__label--yoko" for="input-16">
									      <span class="input__label-content input__label-content--yoko">Name</span>
									    </label>
								    </span>
								 </div>
								 <div class="small-12 text-center ">
								    <span class="input input--minoru">
									    <input class="input__field input__field--yoko" type="text" id="input-16" name="fPhone" >
									    <label class="input__label input__label--yoko" for="input-16">
									      <span class="input__label-content input__label-content--yoko">Phone</span>
									    </label>
								    </span>
								</div>
								<div class="small-12 text-center ">
								    <span class="input input--minoru">
									    <textarea class="input__field input__field--yoko" rows="3" cols="30" type="text" id="input-16" name="fContent"></textarea>
									    <label class="input__label textarea__label--yoko" for="input-16">
									      <span class="input__label-content input__label-content--yoko">Message</span>
									    </label>
								    </span>
							    </div>
							    <div class="small-12 text-center height250 ">
								    <button type="submit" class="sub-button text-upp" name="fInsert">Send Message</button>
							    </div>
							</section>            
				    	</form>
				    	<?php 
							if (isset($_POST['fInsert'])) {
								$fbName = $_POST['fName'];
								$fbPhone = $_POST['fPhone'];
								$fbContent = $_POST['fContent'];
								$query = "INSERT INTO feedback VALUE('$fbName','$fbPhone','$fbContent')";
								mysqli_query($link,$query);
							}
						?>
				    	
				    	
				</div>
				<!-- End headline-->

				<!-- Footer -->
				<footer>
					<h3 class="text-center margin-0 text-upp">&copy;copyright 2018 DRAKE LEE</h3>
					<div class="social flex-center">
						<a href="https://www.facebook.com/DrakeLeeBA" class="social-icon"><i class="fab fa-facebook-f"></i></a>
						<a href="https://plus.google.com/u/0/+DrakeLee" class="social-icon"><i class="fab fa-google-plus-g"></i></a>
						<a href="https://www.linkedin.com/in/drake-lee-b50023143/" class="social-icon"><i class="fab fa-linkedin-in"></i></a>
					</div>
				</footer>
				<!-- End Footer -->

			</div>
		</div>
	</div>

	

</section>
<!-- Preloader -->
	<div id="preloader">
        <div id="loader"></div>
    </div>
	<script src="js/foundation.min.js"></script>
	<script src="js/vendor/jquery.js"></script>
	<script src="js/profile.js"></script>
	<script src="js/main.js"></script>
<!-- End Contact -->

</body>
</html>