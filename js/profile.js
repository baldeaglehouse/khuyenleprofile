jQuery(document).ready(function($) {
	/* Scroll to Persional  */
	$("#per").click(function(e) {
	 	e.preventDefault();
    $('html, body').animate({
        scrollTop: $("#perr").offset().top
    }, 1000);
	});
	/* Scroll to Specialized Skills */
	$("#spec").click(function(e) {
	 	e.preventDefault();
    $('html, body').animate({
        scrollTop: $("#specc").offset().top
    }, 1000);
	});
	/* Scroll to soft Skills */
	$("#soft").click(function(e) {
	 	e.preventDefault();
    $('html, body').animate({
        scrollTop: $("#softt").offset().top
    }, 1000);
	});
	/* Scroll to soft Skills */
	$("#exp").click(function(e) {
	 	e.preventDefault();
    $('html, body').animate({
        scrollTop: $("#expp").offset().top
    }, 1000);
	});

	/* Scroll to soft Skills */
	$("#con").click(function(e) {
	 	e.preventDefault();
    $('html, body').animate({
        scrollTop: $("#conn").offset().top
    }, 1000);
	});

	/* Add Fixed Header */
window.onscroll = function() {scrollFunction()};
function scrollFunction() {
    if (document.body.scrollTop > 877 || document.documentElement.scrollTop > 877) {
        $("header").addClass("headerFixed");
    } else {
        $("header").removeClass("headerFixed");
    }
}

 });

var $document = $(document),
    $pre = $('#pre'),
    $itk = $('#itk'),
    $ctk = $('#ctk'),
    $lea = $('#lea'),
    pre = 'pre' ,
    itk = 'itk' ,
    ctk = 'ctk' ,
    lea = 'lea' ;


$document.scroll(function() {
  $pre.toggleClass(pre, $document.scrollTop() >= 950);
  $itk.toggleClass(itk, $document.scrollTop() >= 950);
  $ctk.toggleClass(ctk, $document.scrollTop() >= 950);
  $lea.toggleClass(lea, $document.scrollTop() >= 950);
});

//Responsive
if (document.documentElement.clientWidth < 736) {
    
    // scripts
    function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}
    window.onclick = function(event) {
  if (!event.target.matches('.nav-btn')) {

    var dropdowns = document.getElementsByClassName("nav");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

}